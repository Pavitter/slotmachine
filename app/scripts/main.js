var gameStart = $('#go');
var gameRunning = $('#running');
var gameWin = $('#win');
var gameOver = $('#lose');
gameRunning.playbackRate=0.5;


var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();


var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://fettblog.eu/fh/numbers.php?jsonp",
            dataType: 'jsonp',
            jsonpCallback: 'callback',
            success: function (data) {
                Mediator.publish('datareceived', data);
            },
            error: function (err) {
				alert("geht ned!");
            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$('html').on('mousedown', startGame);
$('html').on('touchend', startGame);
$('html').on('keydown', startGame);

Mediator.subscribe('datareceived', function (data) {
    startAnimation(data);
});

function startGame() {
	if((parseInt($("#life").text()) > 0)
	&& (parseInt($("#coins").text()) == 0)){
		$("#life").text(parseInt($("#life").text())-1); 
		$("#coins").text("2"); 
	}
	
	if((parseInt($("#coins").text()) >= 1)
	&& animationRunning == false){
		
		$("#coins").text(parseInt($("#coins").text())-1); 
		Mediator.publish('inputGiven');
	}
}

var result;
var animationRunning = false;

function startAnimation(data){
	var slots = data[0]["slots"];
	var slotsClass = new Array();
	 result = data[0]["result"];
/* */console.log("ergebnis: " + slots[0] + " " + slots[1] + " " + slots[2] + " : " + result);
	
	//0 = cherry, 1 = enemy, 2 = carrot, 3 = star
	for(var i = 0; i < 3; i++){
		if(slots[i] == 0){
			slotsClass[i] = "slotPicture-cherry";
		}else if(slots[i] == 1){
			slotsClass[i] = "slotPicture-enemy";
		}else if(slots[i] == 2){
			slotsClass[i] = "slotPicture-carrot";
		}else if(slots[i] == 3){
			slotsClass[i] = "slotPicture-star";
		}
	}
	
	requestAnimationFrame(function(){
		$("#s1").attr("class", "");
		$("#s2").attr("class", "");
		$("#s3").attr("class", "");
		
		$("#s1").addClass("slotPicture animate " + slotsClass[0]);
		$("#s2").addClass("slotPicture animate " + slotsClass[1]);
		$("#s3").addClass("slotPicture animate " + slotsClass[2]);
		
		$("#s1").one("webkitAnimationEnd", endAnimation1);
		$("#s2").one("webkitAnimationEnd", endAnimation2);
		$("#s3").one("webkitAnimationEnd", endAnimation3);
		
		animationRunning = true;
		gameStart[0].play();
		gameRunning[0].play();
	});
	

}

function endAnimation1(){
	requestAnimationFrame(function(){
		$("#s1").removeClass("animate");
	});
}

function endAnimation2(){
	requestAnimationFrame(function(){
		$("#s2").removeClass("animate");
	});
}

function endAnimation3(){
	requestAnimationFrame(function(){
		$("#s3").removeClass("animate");
	});
	
	gameRunning[0].pause();
	
	if(result > 0)
		gameWin[0].play();
		
	if((parseInt($("#life").text()) <= 0)
	&& (parseInt($("#coins").text()) <= 0)){
		gameOver[0].play();
	}
	
	$("#coins").text(parseInt($("#coins").text()) + result); 
	result = 0;
	animationRunning = false;
}




 